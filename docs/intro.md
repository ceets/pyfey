PyFey v0.0.2

PyFey is an interpreter for PyWright scripts, intended to separate
different functions of PyWright (Lexing and parsing, state handling,
and GUI) into clearly distinct pieces of code that are somewhat 
interchangeable.

Main working pieces:

 - feyparser.py: take in a raw string of text and output
a series of 'Command' objects. These objects represent the meaning
of what the line wants you to do. 
		
 - commands.py: contains a series of command objects. 
These are heavily coupled with the first words you 
would see in each PyWright script: set, char,
mus, pos, etcetera, however they may differ
as commands get increasingly complex.
Commands serve as the communication bridge between the Parser()
and the FeyEngine(), which only handles game elements in the abstract.

- feyengine.py: unlike previous pieces, pyengine.PyEngine() is intended
to store state about the current case. It's completely independent form GUI
 - apart from x,y values present in the script itself.
 It exposes its state to its user, however that state should not be modified
 except through public methods like read_line().
 In the future, it will implement a proper event system for each type of action
 for ease of use.
 
 - PyFey.py: main file, mostly used to exemplify how pyengine.py might be used
 by a GUI application. Uses thorpy as a GUI library on top of PyGame.
 
 So:
 PyFey.py uses a FeyEngine() instance, 
 FeyEngine() uses a feyparser.Parser().
 
the Parser() takes in text and outputs commands, 
 and the FeyEngine() exposes its own state + will publish events through an event system.
 
 
 Design goals:
 
 - Maintainable code
 - Small, independent modules
 - Reducing use and abuse of reflection
 - Facilitate implementing new features
 - Separate business logic from the GUI (aka PyGame)
 
 Existing features:
 Parser:
 - Identifies '"..."' dialogue lines, 'char [character]', 'set [var] [value]', 'addev [ev_name]' and more
 - Can do basic script stacking with 'include' and script switching with 'script' - does not clear the state after switch tho
 - Ignores unknown symbols
 - Several exceptions
 
 Engine:
 - Maintains a symbol table of variables, an evidence list and a character list,
 complete with appropriate objects as defined in feyobjects.py.
 - Can load in a script, iterate through it and notify subscribers after any event is over

# Case structure
- the main case file is called "intro", "intro.txt", or "intro.script.txt" (see `FeyEngine.open_game_from_title()`)
    - new cases should prefer "intro.txt", other naming conventions are deprecated
