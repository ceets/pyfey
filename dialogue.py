import textcommands

# TODO: this is doing lexing work. Should be handled by the lexer
    
'''
Output a sequence of DialoguePart objects (like {p60} and {next}).
Object may be COMMAND or TEXT (to be printed without further formatting)
'''
def format(string):
    partitions = []
    current = ''
    open_bracket_pending = False
    for c in string:
        if(c == '{'):
            if not open_bracket_pending:
                if(len(current)):
                    partitions.append(textcommands.SimpleDialogueCommand.create_from_line(current))
                open_bracket_pending = True
                current = ''+ c
            else:
                raise Exception("Trailing brackets in string")
        elif(c == '}'):
            if open_bracket_pending:
                current = current + c
                partitions.append(textcommands.DialogueFlagCommand.create_from_line(current))
                open_bracket_pending = False
                current = ''
            else:
                raise Exception("Trailing brackets in string")
        else:
            current = current + c
    if len(current):
        if(open_bracket_pending):
            raise Exception("open bracked pending")
        elif current[0]=='{' and current[-1] == '}':
            partitions = partitions +  [textcommands.DialogueFlagCommand.create_from_line(current)]
        else: partitions = partitions + [textcommands.SimpleDialogueCommand.create_from_line(current)]
    return partitions
