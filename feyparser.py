import re
import itertools
import commands
class ParseException(Exception):
    pass
class UnimplementedException(Exception):
    pass

from feylexer import get_words
from feylexer import first_word

'''
CommandParser can parse a line consisting of a [key] word followed by arguments.
'''
#TODO: too much code alert
class CommandParser():
    #TODO: after fixing too much code, allow returning multiple actions
    def parse(self, line):
        return commands.parse(line)
        
       
class DialogueParser():
    regex = r'\s*\"[^\"]+.*'
    '''
    Return DialogueCommand if it matches. Else return none
    '''
    def parse(self, line):
        import dialogue
        match = re.match(self.regex, line.strip())
        if not match:
            return None
        return commands.PrintDialogueCommand(dialogue.format(line))


'''
Parser takes in tokens from the lexer and transforms 
them into 'actions', as defined in actions.py.
'Commands' are just bundles of data specific to some particular engine action,
like adding evidence to the inventory or rendering a certain emotion.
TODO: the huge class variable 'commands' ought to be set elsewhere, so that we're getting
the actions AND the commands from the same place
'''
class Parser():
    reserved_tokens = ["$", "&", "+", "-", "?", "\"","\\"]

    def __init__(self, string_sequence):
        self.lines = self.cleanup_lines(string_sequence.splitlines())
        self.command_parser = CommandParser()
        self.dialogue_parser = DialogueParser()
        self.current_index = 0

    '''
    Return a list of actions depending on the token sequence.
    If token is not recognized, return the empty list.
    '''
    def process_line(self, line):
        return self._process_line(line)

    def _process_line(self, line):
        line = line.strip()
        if not len(line): 
            raise ParseException("Line has no content")
        parse = self.dialogue_parser.parse(line)
        if(parse): return parse
        parse = self.command_parser.parse(line)
        if(parse): return parse

        raise UnimplementedException("Don't know how to parse this:" + line)

    @staticmethod     
    def remove_comment(line):
        line = line.split("//",1)[0].strip()
        return line

    @classmethod
    def cleanup_lines(cls,lines):
        newlines = []
        for line in lines:
            line = line.strip()
            line = cls.remove_comment(line)
            if(line and not line == ''):
                newlines.append(line)
        return newlines

    def parse_multiline(self):
        all_actions = []
        self.lines = self.cleanup_lines(self.lines)
        for line in self.lines:
            all_actions.append(self.process_line(line))
        
        return all_actions

class FeyParser:
    def parse(self, string_sequence):
        # make parser from tokens
        # TODO: make this better lmfao
        parser = Parser(string_sequence)
        return parser.parse_multiline()

