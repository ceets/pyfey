set _cr_button false
bg bigblack 
fg chapter1 fade x=50 y=260
mus Turnabout Scapegoat - Intro.ogg
pause 80
bg nieville-snow fade stack x=-256 name=nie
pause 80
scroll x=256 name=nie
pause 100
bg intro-flashbacks/intro-1 fade stack
fg chapter1 x=50 y=260
pause 120
scroll x=-180 intro-1
pause 200
scroll x=-180 intro-1
pause 120
bg intro-flashbacks/intro-2 stack
fg chapter1 x=50 y=260
"{f}{next}"
sfx /awe.ogg
pause 60
bg bigblack
fg chapter1 x=50 y=260
sfx /table slam.ogg
"{s}{next}"
pause 70
bg intro-flashbacks/intro-3 stack fade
fg chapter1 x=50 y=260
pause 70
"{sound /null}{c900}One down...{p60}{n}                   ...one to go...{p120}{next}"
bg bigblack
fg chapter1 x=50 y=260
pause 30
mus Weathertop.ogg
pause 300
"{sound /null}{c900}So.{p30} You came.{p60}{next}"
"{sound /null}{f}{s 10 10}Nyaahhh!{p30}{n}Y-{p10}you gave me quite a shock{n}there!{p80}{next}"
"{sound /null}Heh,{p10} y-you look familiar.{p20}{n}H-have we met?{p80}{next}"
"{sound /null}{c900}Do you know why I called you{n}here?{p100}{next}"
"{sound /null}N-no, I-{p40}{n}Who are you?!{p100}{next}"
"{sound /null}{c900}.{p10}.{p10}.{p10}.{p10}.{p10}.{p10}{p30}{n}An old friend.{p100}{next}"
"{sound /null}{c900}Don't you recognise me?{p100}{next}"
"{sound /null}It's too dark.{p30}{n}I can't see your face.{n}{p30}But that scar...{p100}{next}"
"{sound /null}{c900}Do you know this place?{p100}{next}"
bg intro-flashbacks/intro-4 fade stack y=-195
fg chapter1 x=50 y=260
mus 19 - Reminiscence ~ The Fire Carves Scars.ogg
pause 60
scroll y=195 intro-flashbacks/intro-4
pause 50
"{sound /null}Um...{p40} Isn't this where that{n}murder occured five years ago?{p100}{next}"
"{sound /null}{c900}Yes.{p60}{n}And as fitting a place as any...{p30}{n}for YOUR death...{p60} "Sir."{p60}{next}"
"{sound /null}"Sir?"{p60}{n}......{p60}{sfx /awe.ogg}{f}!{s}{p30}{next}"
bg intro-flashbacks/intro-5
fg chapter1 x=50 y=260
"{sound /null}{f}You!{p10} It can't be!{p70}{next}"
"{sound /null}{c900}Goodbye,{p30} "Sir."{p60}{next}"
bg bigblack 
fg chapter1 x=50 y=260
sfx /slash.ogg
"{s}{next}"
pause 50
sfx /bodyfall.wav
"{s}{next}"
pause 70
bg bigblack 
bg intro-flashbacks/intro-6 fade stack y=-192
fg chapter1 x=50 y=260
pause 100
scroll y=192 intro-flashbacks/intro-6
pause 60
"{sound /null}Y-{p30} Y-{p20} {f}You!!{p100}{next}"
"{sound /null}N-{p20} Now I...{p20} know who you are!{p100}{next}"
"{sound /null}H-{p20} How could you...{p80}{next}"
mus Turnabout Scapegoat - Mystery Tune.ogg
"{sound /null}...V--{p30} V--{p90}{next}"
bg bigblack 
fg chapter1 x=50 y=260
sfx /bodyfall.wav
"{s}{next}"
pause 115
mus 

script scene2