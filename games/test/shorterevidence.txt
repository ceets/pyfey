//EVIDENCE

set attorneybadge_name Attorney's Badge
set attorneybadge_pic attorneybadge
set attorneybadge_desc My all-important badge. It shows I'm a defense attorney.

set magatama_name Magatama
set magatama_pic magatama2
set magatama_desc Originally a gift from Maya. Allows me to see the secrets in people's hearts.

set snowaffidavit_name Affidavit
set snowaffidavit_pic envelope
set snowaffidavit_desc Received from Detective Snow. Show to "Jake" for autopsy report.

set autopsy_name Grossberg's Autopsy Report
set autopsy_pic newreport
set autopsy_desc Est. Time of Death: Mar 27, Midnight.{n}Cause of death: Stab wound to the heart.
set autopsy_check check_g-autopsy

set crimephoto_name Crime Photo
set crimephoto_pic bodyphoto
set crimephoto_desc Photograph of the crime scene. Touch the Examine button for details.
set crimephoto_check check_cs-photo

set prosecutorbadge_name Prosecutor's Badge
set prosecutorbadge_pic erolbadge
set prosecutorbadge_desc Found at the crime scene. Owner unknown.

//PROFILES

set maya$_name Maya Fey
set maya$_pic maya
set maya$_desc Age: 19{n}Gender: Female{n}My assistant and a disciple in the Kurain Chanelling Technique.

set snow$_name Robert Snow
set snow$_pic snow
set snow$_desc Age: 27{n}Gender: Male{n}Detective in charge of the initial investigation. Requires straining effort to talk to him.

set grossberg$_name Marvin Grossberg
set grossberg$_pic grossberg
set grossberg$_desc Age: deceased{n}Gender: Male{n}The victim. He used to be Mia's mentor.

set jake$_name Jake Marshall
set jake$_pic jake
set jake$_desc Age: 36{n}Gender: Male{n}Became a detective again due to the SL-9 resolution. Still thinks he's a cowboy.

set mysteryman$_name ???
set mysteryman$_pic mysteryman
set mysteryman$_desc Age: ??{n}Gender: Male{n}A strange man we encountered at the crime scene. Who is he?

set vinnieyoung$_name Vincenzo Cicatrice
set vinnieyoung$_pic vinnieyoung
set vinnieyoung$_desc Age: 28{n}Gender: Male{n}Me. A defense attorney of 5 years experience.

set grossbergyoung$_name Marvin Grossberg
set grossbergyoung$_pic grossberg
set grossbergyoung$_desc Age: 63{n}Gender: Male{n}My boss and mentor. He thinks we're best friends.

set erolyoung$_name Erol Brisbane
set erolyoung$_pic erol
set erolyoung$_desc Age: 25{n}Gender: Male{n}My client for this trial. Accused of pushing his friend off a cliff.

set crane$_name Jack Crane
set crane$_pic crane
set crane$_desc Age: deceased{n}Gender: Male{n}The victim. A friend of the defendant. He was pushed off a cliff.
