import commands
'''
Object representing evidence and its id, assets, etc
Does not modify symbol table.
They are not intended to perform any GUI actions, but to provide a 
friendly interface for the GUI to extract data.
'''

class Evidence:
    def __init__(self, var_name, symbol_table, menu_type):
        self.var_name = var_name
        if (menu_type == "evidence" or not menu_type): menu_type = ''

        # get keys to find this evidence's properties in symbol table
        pic = var_name + menu_type + "_" + "pic"
        name = var_name + menu_type + "_" + "name"
        desc = var_name + menu_type + "_" + "desc"

        try:
            self.pic = symbol_table[pic]
            self.name = symbol_table[name]
            self.desc = symbol_table[desc]
        except:
            raise Exception("Properties of this piece of evidence are missing. Remember to add a name, pic and description.")
    
    def __str__(self) -> str:
        name = "Name:"+ self.name + "\n Pic:"+ self.pic +"\nDescription: " + self.desc 
        return name 
        
        
class Character:
    def __init__(self, name, emo, sprite_file):
        self.name=name
        self.emo=emo
        self.sprite_file = sprite_file
    def __str__(self) -> str:
        name = "Name:"+ self.name 
        return name