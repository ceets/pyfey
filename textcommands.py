class DialogueFlagCommand():
    first_word = 'script'
    def __init__(self, _comm,_args) -> None:
        self.comm = _comm
        self.args = _args

    '''
    Examples:
    {p80}, {next}
    '''
    @staticmethod
    def create_from_line(line):
        # remove curly brackets
        line = line[1:-1]
        if(line[0]=='p'):
            duration = float(line[1:])
            return DialogueFlagCommand('p', [duration])
        
        elif(line[:4]=='next'):
            if line != 'next':
                raise Exception("Did you attempt a 'next' command? you wrote it weird!")
            return DialogueFlagCommand('next', None)

        else: return DialogueFlagCommand(line, None)

    
'''
Piece of a complex dialogue command that is just a chunk of text.
'''
class SimpleDialogueCommand():
    first_word = 'dialogue'
    def __init__(self, final_string) -> None:
        self.value = final_string

    def __str__(self) -> str:
        return self.value

    @staticmethod
    def create_from_line(line):
        return SimpleDialogueCommand(line)


