"""
Interface through which feyparser communicates with the engine.
'Commands' are just bundles of data specific to some particular engine action,
like adding evidence to the inventory or rendering a certain emotion.
"""
from platform import mac_ver
from feylexer import get_words
from feylexer import first_word

class Command():
    pass

# List of all wright script commands - including unimplemented ones.
# if matching with classes fails, use these to identify unimplemented commands.
all_commands = ['absvar', 
    'addev', 'addvar', 'bemo', 'bg', 'callpresent', 
    'callpress', 'casemenu', 'char', 'clear', 'clearcross', 
    'controlanim', 'cross', 'cross_restart', 'debug', 'delete', 'delev', 
    'delflag', 'divvar', 'emo', 'endcross', 'endscript', 'ev', 'examine',
    'examine3d', 'exit', 'exportvars', 'fade', 'fg', 'filewrite', 
    'flag', 'forgetlist', 'forgetlistitem', 'framerate', 'game', 
    'gamemenu', 'gchildren', 'getprop', 'getvar', 'globaldelay', 'goto', 
    'grey', 'gui', 'importvars', 'invert', 'is', 'is_ex', 'isempty', 'isnot',
    'isnotempty','isnumber', 'joinvar', 'label', 'li', 'list', 'lo', 'loadgame', 
    'localmenu', 'menu', 'mesh','movie', 'mulvar', 'mus', 'next_statement', 
    'noflag', 'nt', 'obj', 'pause', 'penalty', 'present',
    'prev_statement', 'print', 'random', 'region3d', 'resume', 'rotate',
    'savegame','screenshot', 'script', 'scroll', 'set', 'set_ex', 
    'set_variables', 'setflag', 'setprop', 'setvar', 
    'setvar_ex', 'sfx', 'shake',  'showlist', 'showpresent', 'showrecord', 
    'statement', 'step', 'subvar', 'surf3d', 
    'textblock', 'textbox', 'timer', 'tint', 'top', 'update_objects', 'waitenter', 'zoom',
    'include'] #'include' didnt use to be a proper command, but a preprocessing flag

'''
Example:
set _music_loop true
set _variable_name 69420
'''
class SetCommand(Command):
    first_word = 'set'
    def __init__(self, var_name, value) -> None:
        if (value == "true"):
            value = True
        elif (value == "false"):
            value = False
        else: self.value = value

        self.var_name = var_name
        self.var_value = value

    def __str__(self) -> str:
        return "Set the variable " + str(self.var_name) + " to " + str(self.var_value)

    @staticmethod
    def create_from_line(line):
        words = get_words(line)
        if(words[0]) != 'set':
            raise Exception("Invalid set command")
        # find value argument, which starts after the first two words and one space
        arg_value_index = line.index(words[1]) + len(words[1]) + 1
        value = line[arg_value_index:]
        return SetCommand(get_words(line)[1], value)



'''
Represents dialogue like this:
"Hello{n}My name is...{p80} the ace attorney{next}"
'''
class PrintDialogueCommand(Command):
    first_word = 'dialogue'
    def __init__(self, subactions) -> None:
        self.subactions = subactions
        printables = filter(lambda a : a.first_word == 'dialogue', self.subactions)
        printables = list(map(lambda p : str(p), printables))
        self.string = "".join(printables)

    def __str__(self) -> str:
        import feyobjects
        printables = filter(lambda a : a.first_word == 'dialogue', self.subactions)
        printables = list(map(lambda p : str(p), printables))
        return "the dialogue is: "+ self.string

class AddEvidenceCommand(Command):
    '''
    Uses preexisting variables to add evidence to the court record.
    EXAMPLE of creating a piece of evidence and showing it onscreen:
    set housefirst_word_pic first_word1
    set housefirst_word_name House Key
    set housefirst_word_desc The first_word to the victim's house
    addev housefirst_word
    ev housefirst_word
    "House first_word added to court record"
    '''
    first_word = 'addev'
    PROFILE_MARKER = '$'
    PROFILE = 'profile'
    EVIDENCE = 'evidence'
    def __init__(self, name, menu_type = EVIDENCE) -> None:
        self.name = name
        self.custom_menu = not (menu_type in [self.PROFILE, self.EVIDENCE])
        self.menu_type = menu_type

    def __str__(self) -> str:
        name = "Add " + self.name + " to the "+str(self.menu_type)  +" menu."
        if(self.custom_menu): name = name + "It's a custom menu."
        return name

    @staticmethod
    def create_from_line(line):
        import re
        words = get_words(line)
        if(len(words) != 3 and len(words) != 2):
            raise Exception("Invalid addev: word length is "+ len(words))
        if(words[0] != 'addev'):
            raise Exception("First word is not 'addev'")
        # TODO: too broad
        if('$' in line):
            return AddEvidenceCommand(words[1], re.match(r'[/S]$', line))
        else: return AddEvidenceCommand(words[1])

'''
Base structure of char:
 char character_name
 nametag=VALUE e=VALUE be=VALUE
 x=VALUE y=VALUE z=VALUE
 name=VALUE pri=VALUE fade stack hide noauto
'''
class AddCharacterCommand(Command):
    first_word = 'char'
    #TODO save and use all parameters
    def __init__(self, name,
            tag=None,
            emotion='normal',
            blink_emotion=None,
            x='center',y='bottom',z=1,
            script_name=None,
            pri=1,
            stack=False,
            hide=False,
            noauto=False) -> None:
        self.name = name
        self.tag = tag if tag else name
        self.emotion=emotion
        self.blink_emotion = blink_emotion
        self.x = x
        self.y = y


    def __str__(self) -> str:
        return self.name

    @staticmethod
    def create_from_line(line):
        words = get_words(line)
        if(words[0] != 'char'):
            raise Exception("First word is not 'char'")
        return AddCharacterCommand(words[1])

'''
INCLUDE:
Push script on the stack and execute all its lines, then return
'''
class IncludeScriptCommand(Command):
    first_word = 'include'
    def __init__(self, script) -> None:
        self.script_name = script

    def __str__(self) -> str:
        return "Include script: " + self.script_name

    def create_from_line(line):
        words = get_words(line)
        if(words[0] != 'include'):
            raise Exception("First word is not 'include'")
        return IncludeScriptCommand(words[1])


'''
Used when the first_word is neither in our commands list, nor
a first_word from previous commands. This means the engine
ought to look it up in the list of macros and
verify it exists.
'''
class MacroCommand(Command):
    def __init__(self, line) -> None:
        self.line = line

    def __str__(self) -> str:
        return "Possible macro: " + self.line

    def create_from_line(line):
        return MacroCommand(line)

'''
TODO: Jumps to script and dismisses the previous one.
'stack' flag not yet implemented. 
'''
class JumpToScriptCommand(Command):
    first_word = 'script'
    def __init__(self, script) -> None:
        self.script_name = script
    def __str__(self) -> str:
        return "Jump to script: " + self.script_name
    def create_from_line(line):
        words = get_words(line)
        if(words[0] != 'script'):
            raise Exception("First word is not 'script'")
        return JumpToScriptCommand(words[1])

class UnimplementedCommand(Command):
    def __init__(self, line) -> None:
        self.line = line
        self.first_word = first_word(line)
    def create_from_line(line):
        return UnimplementedCommand(line)
    def __str__(self) -> str:
        return "Unimplemented command: "+self.line

'''
provide list of classes where we know which first word to expect
'''
def implemented_commands():
    return [SetCommand,
    PrintDialogueCommand,
    AddEvidenceCommand,
    JumpToScriptCommand,
    IncludeScriptCommand,
    AddCharacterCommand]


def parse(line):
    #TODO: generalize commands better

    #see if its an implemented command
    for action_type in implemented_commands():
        if(action_type.first_word == first_word(line)):
            return action_type.create_from_line(line)

    # see if its an unimplemented command
    for command in all_commands:
        if (command == first_word(line)):
            return UnimplementedCommand.create_from_line(line)

    #then tell the engine to search it as a macro
    return MacroCommand.create_from_line(line)




