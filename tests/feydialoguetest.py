import unittest
import commands
import dialogue
class TestDialogue(unittest.TestCase):
    '''
    '''
    def test_parse_dialogue(self):
        import textcommands
        string = '"garñbañge{p80}basura{alguito}{algomas}masbasura"'
        self.assertEquals(len(dialogue.format(string)),6)
        for part in dialogue.format(string):
            self.assertTrue(isinstance(part, textcommands.SimpleDialogueCommand) or isinstance(part,textcommands.DialogueFlagCommand))
    
    '''
    '''
    def test_parse_incorrect_dialogue(self):
        string1 = '"{{{p80}basura{alguito}{algomas}masbasura{"'
        string2 = '"garñbañge{p80}basura{alguito}{algomas}masbasura{"'
        
        with self.assertRaises(Exception):
            diag = dialogue.format(string1)
        
        with self.assertRaises(Exception):
            diag = dialogue.format(string2)
    
    '''
    '''
    def test_use_dialogue_object(self):
        import textcommands
        import dialogue
        string = '"garñbañge{p80}basura{alguito}{algomas}masbasura"'
        subactions = dialogue.format(string)
        self.assertTrue(isinstance(subactions[0], textcommands.SimpleDialogueCommand))
        self.assertTrue(isinstance(subactions[1], textcommands.DialogueFlagCommand))
        self.assertTrue(isinstance(subactions[2], textcommands.SimpleDialogueCommand))
        self.assertTrue(isinstance(subactions[3], textcommands.DialogueFlagCommand))
        self.assertTrue(isinstance(subactions[4], textcommands.DialogueFlagCommand))
        self.assertTrue(isinstance(subactions[5], textcommands.SimpleDialogueCommand))

        self.assertEquals(subactions[1].args,[80.0])
        self.assertEquals(subactions[3].args,None)
            


