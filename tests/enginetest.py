import unittest

'''
'''
class TestEngine(unittest.TestCase):

    textaddev = '''addev 4luminol
addev 4videotape
addev 37
addev alarm
addev aluadone
addev article
addev arumajikistamp
addev autograph
addev ball
addev bloodyace
addev bullet
addev bullet_001
'''
    textremington = '''statement BBB
"I don't even own a Remington Rifle".

statement CCC
"I also have an airtight alibi."

endcross

label panic
"Oh shoot, I'm not sure how to break him."
"Just keep on pressing!"
goto blah

label press AAA
"But how can you claim that with no evidence?"
"Just let me continue my testimony and I'll show you the evidence!"
goto blah
'''

    def test_parse(self):
        from feyengine import FeyEngine

        engine = FeyEngine()
        engine.open_game_from_title("test")
        line = engine.read_line()
        lines = []
        while(line):
            lines.append(line)
            line = engine.read_line()

        self.assertEquals(0, len(engine.script_stack))
        self.assertEquals(lines[0], 'the dialogue is: "Begin execution of Contempt of Court"')
        self.assertTrue('Include script: shorterevidence' in lines)
        self.assertEquals(lines[3], 'maya')
        self.assertEquals(lines[4], 'the dialogue is: "that should have spawned maya"')
        self.assertEquals(len(engine.symbol_table.items()),47)
        self.assertEquals(engine.symbol_table["prosecutorbadge_pic"], "erolbadge")
        pass

    def test_parse_with_include(self):
        from feyengine import FeyEngine
        import commands

        engine = FeyEngine()
        engine.open_game_from_title("test")
        self.assertEquals(1, len(engine.script_stack))
        lines = []
        line = engine.read_line()
        while(line):
            line = engine.read_line()


        self.assertEquals(0, len(engine.script_stack))
        self.assertEquals(engine.symbol_table["prosecutorbadge_pic"], "erolbadge")
        self.assertEquals(engine.symbol_table["maya$_name"], "Maya Fey")

        evidence = list(map(lambda e:e.name, engine.player_evidence))
        expected_evidence = ["Attorney's Badge", 'Magatama', 'Maya Fey']
        self.assertEquals(expected_evidence, evidence)
        pass

    def test_set_command(self):
        from feyengine import FeyEngine
        import commands

        engine = FeyEngine()
        set_action = commands.SetCommand( 
            '_music_loop', 
            'SomeMusic.ogg')
        line = engine.read_line(set_action)
        self.assertEquals('SomeMusic.ogg', engine.symbol_table['_music_loop'])
        pass


    def test_add_evidence(self):
        from feyengine import FeyEngine
        import commands

        engine = FeyEngine()
        self.assertEquals([], engine.player_evidence)

        # set badge evidence variables
        set_action = commands.SetCommand( 
            'badge_name', 
            "Phoenix's Badge")
        line = engine.read_line(set_action)
        set_action = commands.SetCommand( 
            'badge_desc', 
            "Proof that I'm a lawyer, somehow.")
        line = engine.read_line(set_action)
        set_action = commands.SetCommand( 
            'badge_pic', 
            "badge.jpeg")
        line = engine.read_line(set_action)
        self.assertEquals(len(engine.symbol_table.keys()), 3)

        set_action = commands.AddEvidenceCommand( 
            'badge')
        line = engine.read_line(set_action)
        self.assertEquals('badge', engine.player_evidence[0].var_name)
        pass

    def test_multiquote(self):
        from feyengine import FeyEngine
        import commands

        engine = FeyEngine()
        self.assertEquals([], engine.player_evidence)

        # set badge evidence variables
        import dialogue
        line = '''{sound /null}{c900}Goodbye,{p30} "Sir."{p60}{next}'''
        set_action = commands.PrintDialogueCommand(dialogue.format(line))
        line = engine.read_line(set_action)
        self.assertEquals(len(engine.symbol_table.keys()), 0)
        self.assertEquals('Goodbye, "Sir."', engine.current_dialogue)
        pass

    


