import unittest
class TestParse(unittest.TestCase):
    textaddev = '''addev 4luminol
    addev 4videotape
    addev 37
    addev alarm
    addev aluadone
    addev article
    addev arumajikistamp
    addev autograph
'''
    textremington = '''statement BBB
"I don't even own a Remington Rifle".

statement CCC
"I also have an airtight alibi."

endcross

label panic
"Oh shoot, I'm not sure how to break him."
"Just keep on pressing!"
goto blah

label press AAA
"But how can you claim that with no evidence?"
"Just let me continue my testimony and I'll show you the evidence!"
goto blah
'''

    def testparse(self):
        from feyparser import Parser
        text = '''clearcourt

                set _judge judge

                startcourt

                judge
                "Stuff is going down in this here court of mine!"

                some other stuff
                '''
        parser = Parser(text)
        multiline_results = parser.parse_multiline()


    def testcommand(self):
        from feyparser import Parser
        text = self.textaddev
        parser = Parser(text)
        multiline_results = parser.parse_multiline()
        expected = ['Add 4luminol to the evidence menu.',
         'Add 4videotape to the evidence menu.', 
         'Add 37 to the evidence menu.', 
         'Add alarm to the evidence menu.', 
         'Add aluadone to the evidence menu.', 
         'Add article to the evidence menu.', 
         'Add arumajikistamp to the evidence menu.', 
         'Add autograph to the evidence menu.']
        text_results = list(map(lambda a : str(a),multiline_results))
        self.assertEquals(text_results, expected)
        # parser = Parser(text)
        # for expected_action in expected:
        #     actions = parser.next_line()
        #     self.assertEquals([expected_action], list(map(lambda a : str(a), actions)))

    def testremington(self):
        from feyparser import Parser
        text = self.textremington
        expected = ["Unimplemented command: statement BBB",
         '''the dialogue is: "I don't even own a Remington Rifle".''', 
        "Unimplemented command: statement CCC", 
         'the dialogue is: "I also have an airtight alibi."', 
         "Unimplemented command: endcross", 
         "Unimplemented command: label panic", 
         'the dialogue is: "Oh shoot, I\'m not sure how to break him."',
         'the dialogue is: "Just keep on pressing!"',
          "Unknown command:goto. Args: ['blah']",
           "Unimplemented command: label press AAA", 
           'the dialogue is: "But how can you claim that with no evidence?"',
           '''the dialogue is: "Just let me continue my testimony and I'll show you the evidence!"''',
          "Unimplemented command: goto blah" ]

        # TODO:line-by-line parsing has been unimplemented
        parser = Parser(text)
        parsedlines = parser.parse_multiline()
        parsedlines[0].first_word == 'statement'
        parsedlines[1].first_word == 'dialogue'
        parsedlines[2].first_word == 'statement'
        parsedlines[3].first_word == 'dialogue'
        
    def test_comments(self):
        from feyparser import Parser
        text = '''//Hi my name is rick
        //Hi my name is comment
        //Hi my name is john
        set _successful_commenting true'''
        expected = ["set set set" ]

        parser = Parser(text)
        parsedlines = parser.parse_multiline()

        # comments are not counted as 'unknown commands'
        self.assertEquals(len(text.splitlines()), 4)
        self.assertEquals(len(parsedlines), 1)

    def test_incorrect_comments(self):
        from feyparser import Parser
        import commands
        text = '''#Hi my name is rick
        #Hi my name is comment
        #Hi my name is john
        set _successful_commenting true'''
        expected = ["set set set" ]

        parser = Parser(text)
        parsedlines = parser.parse_multiline()

        #unknown first word interpreted as a macro
        isinstance( parsedlines[0],commands.MacroCommand)
        isinstance( parsedlines[3],commands.SetCommand)

        # comments are not counted as 'unknown commands'
        self.assertEquals(len(text.splitlines()), 4)
        self.assertEquals(len(parsedlines), 4)

    # def testremington_multiline(self):
    #     from feyparser import Parser
    #     text = self.textremington

    #     parser1 = Parser(text)
    #     parser2 = Parser(text)

    #     results_multi = parser2.parse_multiline()
    #     results = []
    #     for expected_action in range(0,13):
    #         actions = parser1.next_line()
    #         results = results + actions

    #     self.assertEquals(
    #         list(map(lambda actions: str(actions),results)), 
    #         list(map(lambda actions: str(actions),results_multi)))

    def testparsemultiline(self):
        from feyparser import Parser
        text = self.textremington
        parser = Parser(text)
        parser.parse_multiline()