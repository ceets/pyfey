import feyparser
import commands
import feyobjects

class FileSystem:    
    '''
    Pywright has weird quirks when opening files. 
    It does things like, when you open a script,
    it tries either [name].txt or [name].script.txt or just [name].
    This class is here to replicate that weird behavior for
    backwards compat.

    folder
        the base folder under all games are expected to be
    title
        name of the game folder. game files are under <folder>/<title>/
    '''
    def __init__(self, folder, title):
        if not folder or not title: raise Exception
        self.folder = folder
        self.title = title

    # TODO: reconsider having the folder parameter. when set bypasses basic class functionality
    #       _get_script_from_name's "folder" behaves like __init__'s "folder/title"
    #       if kept, consider renaming to path
    def _get_script_from_name(self, name, folder = None):
        '''
        Reads game using the FileReader.
        Tries 'name.txt', 'name.script.txt' and 'name' in that order.
        Uses the games folder.
        '''
        if not folder:
            folder = self.folder + "/" + self.title
        try:
            script = self.get_game_from_path('./{}/{}.txt'.format(folder, name))
        except(Exception):
            try:
                script = self.get_game_from_path('./{}/{}.script.txt'.format(folder, name))
            except(Exception):
                script = self.get_game_from_path('./{}/{}'.format(folder, name))
        return script

    def get_game_from_path(self, path):
        file = open(path, "r")
        return file.read()

    '''
    Verify that character sprite exists, else throw.
    Should use a library to try different file endings for sprites
    '''
    def get_character_sprite(self, name, emo):
        path1 = "./art/port/" + name + "/" + emo + ".png"
        path2 = "./art/port/" + name + "/" + emo + "(talk)" + ".png"
        path3 = "./"+ self.folder + "/" + self.title +"/art/port/"+ name + "/" + emo + ".png"
        f = None
        chsn_path = None
        for path in [path1, path2]:
            try:
                f = open(path, "rb")
                chsn_path = path 
                f.close()
                break
            except(FileNotFoundError): pass
        if not chsn_path: raise FileNotFoundError
        return chsn_path


        

class NoMoreScripts(Exception):
    pass

'''
Handles the state of the game and uses the FeyParser, 
encapsulating it from the GUI.
Provides output and raises events to the GUI.
Each instance of FeyEngine represents One game.
'''
class FeyEngine:
    def __init__(self) -> None:
        self.parser = feyparser.FeyParser()
        self.symbol_table = {}
        self.player_evidence = []
        self.script_stack = []
        self.current_dialogue = None
        self.characters = []
        self.subscribers = []
        pass

    '''
    So full list is displayed on the debugger, as opposed to iter(list)
    '''
    class Iterator():
        def __init__(self, finite_seq) -> None:
            self.seq = finite_seq
            self.index = 0
        def __len__(self):
            return len(self.seq)
        def __next__(self):
            try:
                ret = self.seq[self.index]
            except:
                raise StopIteration
            self.index += 1
            return ret
        
    def _append_game(self, script_text):
        '''
        take script, turn it into iterable and append it to stack
        '''
        parsed_script = self.parser.parse(script_text)
        line_iterator = self.Iterator(parsed_script)
        self.script_stack.append(line_iterator)

    def next(self):
        try:
            iter = self.script_stack[-1]
            return next(iter)
        except Exception:
            return None

    def _get_action_from_stack(self):
        '''
        read next action from script stack.
        if there's no more lines in the current script, use the next one.
        Returns the read action
        Throws AttributeError if no scripts are left
        '''
        action = self.next()
        while not action:
            # last script ran out, remove script
            try:
                self.script_stack.pop()
            except:
                raise NoMoreScripts()
            # get a new action
            action = self.next()
        return action

    def _add_evidence(self, evidence_name, menu):
            ev = feyobjects.Evidence(evidence_name, self.symbol_table, menu)
            self.player_evidence.append(ev)

    def _add_character(self, name, emo):
            sprite_file = self.file_system.get_character_sprite(name, emo)
            char = feyobjects.Character(name, emo, sprite_file)
            self.characters.append(char)



    def _include_script(self,script):
        script = self.file_system._get_script_from_name(script)
        self._append_game(script)
        pass

    def _replace_script(self,script):
        # remove script
        self.script_stack.pop()
        self._include_script(script)

    def _react_to_action(self, action):
        if(isinstance(action,commands.SetCommand)):
            self.symbol_table[action.var_name] = action.var_value
        if(isinstance(action,commands.AddEvidenceCommand)):
            self._add_evidence(action.name, action.menu_type)
        if(isinstance(action,commands.IncludeScriptCommand)):
            self._include_script(action.script_name)
        if(isinstance(action,commands.JumpToScriptCommand)):
            self._replace_script(action.script_name)
        if(isinstance(action,commands.PrintDialogueCommand)):
            self.current_dialogue = action.string
        if(isinstance(action,commands.AddCharacterCommand)):
            self._add_character(action.name, action.emotion)
        return action if action else None

    '''
    PUBLIC INTERFACE
    '''

    # TODO: reconsider having the action parameter and associated default value logic
    #       action is only ever passed in for testing, and handling the "correct" behavior as a special case muddies the code
    # TODO: rename the method to disclaim its side effects
    def read_line(self, action = None):
        if not action: 
            try:
                action = self._get_action_from_stack()
            except(NoMoreScripts):
                return None
        result = self._react_to_action(action)
        self.notify_subscribers(action)
        return str(result)

    # file system stuff

    '''search folder under the games folder Or the provided folder name '''
    # TODO: reconsider having this method
    #       a FeyEngine instance is supposed to represent *one* game, but this method lets you load in as many games as you want
    #       ideally this logic would get moved to __init__()
    def open_game_from_title(self, title, folder = 'games'):
        self.file_system = FileSystem(folder, title)
        self.current_game = title
        script = self.file_system._get_script_from_name("intro")
        self._append_game(script)

    def subscribe_to_all_actions(self, react_func):
        self.subscribers.append(react_func)

    def notify_subscribers(self, action):
        for react_func in self.subscribers:
            react_func(action)


