
def get_words(line):
    return line.split(" ")

def first_word(line):
    return get_words(line)[0]