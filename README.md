Version 0.0.2(alpha)

To run a test script, go to the games/test folder and edit intro.txt
The last handful of
1. executed lines or 'actions'
2. evidence items
3. variables set in the symbol table

will be displayed onscreen.

The GUI requires the art pack (composed of the folders art, music, sfx) to work. you can find it online.

You can find further documentation in the docs folder.

# How to run

## Installing the dependencies
You'll need Python 3.9.7+ and pip3.
See the [official download page](https://www.python.org/downloads/) for instructions appropiate to your operating system.

You can then install module dependencies by running `pip3 install thorpy pygame` from a terminal.

## Running the engine
You can run the engine by running `python3 PyFey.py` in a terminal *from the project folder*.

## Running the tests
You can run the unit tests by running `python3 -m unittest tests/*.py` in a terminal *from the project folder*.
You can run the unit tests from a different location by adjusting the last argument so that it points to the same folder.

# Replicating the development environment
1. Install VSCode with the "Python" and "Pylance" IDE extensions.
2. Go to the "Testing" tab and *set up the testing environment*.
    - TODO: break down testing environment setup

# Project structure
- `art/` default art for the game, available from every casefile
    - `3d/`
- `docs/`
- `examples/`
- `games/test/`
- `music/`
- `sfx/`
- `tests/`
- `commands.py`
- `dialogue.py`
- `feyengine.py`
- `feylexer.py`
- `pablada.py`
- `PyFey.py` **main**
- `test-thorpy.py`
- `textcommands.py`
