import pygame
import sys
import queue
import thorpy
import commands
import feyengine  
import textwrap  

def wrap_text(text_obj, width=30):
    return "\n".join(textwrap.wrap(text_obj, width))

class View: 
    def __init__(self) -> None:
        self.app = None
        self.menu = None
        self.engine = feyengine.FeyEngine()
        self.box = None
        self.evidence = None
        self.symbols = None
        self.textbox = None
        self.box_list = []
        self.evidence_list = []
        self.box_controller = None
        self.screen = None

    def execute_action(self, action):
        if isinstance(action, commands.PrintDialogueCommand):
            return 'pause'
        if isinstance(action, commands.SimpleDialogueCommand):
            return 'pause'
    # this is more of a controller thing
    def launch_menu(self):

        if not self.box_controller:
            self.box_controller = DialogueBoxController(self, self.engine)
            self.textbox = self.box_controller.create_dialogue_box('')
            self.menu.add_to_population(self.textbox) 
        
        if not self.evidence:
            self.evidence_controller = EvidenceController(self, self.engine)
            self.evidence = self.evidence_controller.create_evidence()
            self.menu.add_to_population(self.evidence)
            self.draw_evidence() 

        if not self.symbols:
            self.symbols = self.create_symbol_table()
            self.menu.add_to_population(self.symbols)
            self.draw_symbol_table()

        if not self.screen:
            self.symbols = self.create_symbol_table()
            self.menu.add_to_population(self.symbols)
            self.draw_symbol_table()
            

        action = self.engine.read_line() # this will notify subscribers including outselves

    def after_all_actions(self, action):
        if(isinstance(action, commands.PrintDialogueCommand)):
            self.box_controller.characters_written = 0
        if(isinstance(action, commands.AddCharacterCommand)):
            if not self.screen:
                self.screen = self.create_screen()
        text=str(action)
        self.box_list += [text]
        self.print_in_box(self.box_list)
        self.box.finish()
        self.update_symbol_table()
        self.evidence_controller.update_evidence()
        if(self.screen):
            self.update_screen()
        self.box_controller._update()
        self.menu.add_to_population(self.symbols)        
        self.menu.add_to_population(self.evidence)  
        self.draw_textbox()
        self.draw_evidence()
        self.draw_symbol_table()
        if(self.screen):
            self.draw_screen()

    #draw
    def draw_textbox(self):
        self.textbox.finish()
        self.textbox.stick_to('screen', 'top', 'top')
        self.textbox.blit()
        self.textbox.update()

    def draw_evidence(self):
        self.evidence.finish()
        self.evidence.stick_to('screen', 'right', 'right')
        self.evidence.blit()
        self.evidence.update()

    def draw_symbol_table(self):
        self.symbols.finish()
        self.symbols.stick_to(self.evidence, 'bottom', 'top')
        self.symbols.blit()
        self.symbols.update()

    def draw_screen(self):
        self.screen.finish()
        screensize = thorpy.get_screen().get_rect().size
        center = (screensize[0]/2, screensize[1]/2)
        self.screen.set_center((center))
        self.screen.blit()
        self.screen.update()

    # create
    def create_symbol_table(self):
        title = [thorpy.make_text("Symbol table", font_size=18)]
        table = list(self.engine.symbol_table.items())
        elements = table[-6:] if len(table) > 6 else table
        elements = list(map(lambda e: thorpy.make_text(wrap_text(str(e))), elements))
        elements.reverse()
        symbols = thorpy.Box(title+elements, size=(200,400))
        return symbols

    def create_screen(self):
        characters = self.engine.characters
        images = []
        for character in characters:
            image = thorpy.Image(character.sprite_file)
            images.append(image)
        screen = thorpy.Box(images, size=(256,192))
        return screen
    
    # update
    def update_symbol_table(self):
        table = self.engine.symbol_table.items()
        text_elems = table
        text_elems = list(map(lambda e: str(e), text_elems))
        self.symbol_list = text_elems[-7:]
        title = self.symbols._elements[0]
        self.symbols.remove_all_elements()
        for text in text_elems:
            self.symbols.add_element(thorpy.make_text(wrap_text(text)), insert = True)
        self.symbols.add_element(title, insert=True)
        _len = len(self.symbols._elements)
        if(_len>1):
            print(_len)

    def update_screen(self):
        characters = self.engine.characters
        images = []
        for character in characters:
            image = thorpy.Image(character.sprite_file)
            images.append(image)
        self.screen.remove_all_elements()
        self.screen.add_elements(images)
        

    def print_in_box(self, text_list):
        title = thorpy.make_text("Commands", font_size=18)
        text_list = list(map(lambda t: thorpy.make_text(wrap_text(t), font_size=14), text_list[-5:] ))
        if not self.box:
            self.sub_box = thorpy.Box(text_list, size = (210, 550))
            self.box = thorpy.Box(elements=[title, self.sub_box], size = (300, 600))
            self.menu.add_to_population(self.box)
        else:
            self.sub_box.remove_all_elements()
            self.sub_box.add_elements(text_list, insert = True)
            self.sub_box.finish()
            self.box.fit_children(margins=(300,300))
            self.box.finish()
        self.box.blit()
        self.box.update()

    def run(self):
        self.application =  thorpy.Application((0,0), flags = pygame.RESIZABLE)
        thorpy.set_theme("simple")
        some_text = thorpy.make_text("Click on the button below to launch menu.", 18)
        button = thorpy.make_button("launch menu", self.launch_menu)
        image = thorpy.Image("pyfey.jpeg")
        image.set_size((500,500))
        background = thorpy.Background(elements=[image,some_text,button])
        thorpy.store(background)

        def video_resize(event, menu):
            menu.blit_and_update()

        self.menu = thorpy.Menu()
        self.menu.add_to_population(background)
        # this one was tough, remember to label params as 'params' or itll fail silently
        background.add_reaction(thorpy.Reaction(
            pygame.VIDEORESIZE,
            video_resize,
            params = {"menu": self.menu}))

        # feyengine
        self.engine.open_game_from_title("test")
        self.engine.subscribe_to_all_actions(self.after_all_actions)
        self.menu.play()

        self.application.quit()

class DialogueBoxController:   
    def __init__(self, view, engine) -> None:
        self.engine = engine
        self.characters_written = 0
        self.framewait = 1
        self.frames_count = 0
        self.view = view
    def _update(self):
        if(self.frames_count % self.framewait == 0):
            text = self.update_contents()
            text_elem = view.textbox._elements[0]
            text_elem.set_text(text)
            view.draw_textbox()
            self.frames_count = 0
        self.frames_count += 1
    def update_contents(self):
        text = self.engine.current_dialogue if self.engine.current_dialogue else ''
        text = text[:self.characters_written+1]
        self.characters_written += 1
        return text
    def create_dialogue_box(self, text):
        title = thorpy.make_text(wrap_text(text), font_size=12)
        dialogue = thorpy.Box([title], size=(400,150))
        thorpy.add_time_reaction(dialogue, self._update)
        return dialogue

class EvidenceController:   
    def __init__(self, view, engine) -> None:
        self.engine = engine
        self.evidence_list = []
        self.view = view

    def _update(self):
        # Evidence is not updated periodically, but after actions occur
        pass

    def update_contents(self):
        table = self.engine.player_evidence
        text_elems = table
        text_elems = list(map(lambda e: str(e), text_elems))
        self.evidence_list = text_elems[-7:]

    def update_evidence(self):
        self.update_contents()
        title = view.evidence._elements[0]
        view.evidence.remove_all_elements()
        for text in self.evidence_list:
            view.evidence.add_element(thorpy.make_text(wrap_text(text)), insert = True)
        view.evidence.add_element(title, insert=True)
        return 

    def create_evidence(self):
        title = [thorpy.make_text("Evidence", font_size=12)]
        evidence = thorpy.Box(title, size=(300,300))
        thorpy.add_time_reaction(evidence, self._update)
        return evidence



view = View()
view.run() 
